const Course = require("../models/Course");

// Create a new course
/*
module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});
	return newCourse.save().then((course, error) => {
		// Course creation failed
		if (error) {
			return false;
		// Course creation successful
		} else {
			return true;
		};
	});
};
*/

// REFACTORED addCourse
/*module.exports.addCourse = (req, user) => {
if (user.isAdmin == true) {*/
module.exports.addCourse = (data) => {
	// User is an admin
	if (data.isAdmin) {
		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});
		// Saves the created object to our database
		return newCourse.save().then((course, error) => {
			// Course creation successful
			if (error) {
				return false;
			// Course creation failed
			} else {
				return true;
			};
		});
	};
	// User is not an admin
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};

// Retrieve ALL courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	});
};

// Retrieve ALL ACTIVE courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};

// Retrieve specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};

// Update a course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	// findByIdAndUpdate(document ID, updatesToBeApplied) 
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error) {
			return false;
		} else {
			return true;
		};
	});
};

// [S40 ACTIVITY] STARTS HERE 
/*
module.exports.archiveCourse = (reqParams, reqBody) => {
	let archivedCourse = {
		isActive: reqBody.isActive	
	};
	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
		if(error) {
			return false;
		} else {
			return true;
		};
	});
};
*/
// [S40 ACTIVITY] ENDS HERE 

// s40 Activity Solution
module.exports.archiveCourse = (reqParams) => {
	let updateActiveField = {
		isActive: false	
	};
	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {
		if(error) {
			return false;
		} else {
			return true;
		};
	});
};
// s40 solution ends here








