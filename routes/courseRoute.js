const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");

const auth = require("../auth");

// Route for creating a course
/*
router.post("/create", (req, res) => {
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
});
*/

// [S39 ACTIVITY STARTS HERE]
// Route for creating a course only for admins 
router.post("/create", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});
// [S39 ACTIVITY ENDS HERE]

// Route for retrieving all the courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all active the courses
router.get("/active", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving specific course
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// [S40 ACTIVITY] STARTS HERE 
/*
router.patch("/:courseId", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});
*/
// [S40 ACTIVITY] ENDS HERE 

// s40 Activity Solution
router.patch("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
});

module.exports = router;
// s40 solution ends here









